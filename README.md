# public
公共模块，封装组件
 **

 **小程序组件** 

   在需要用的页面的json文件中写下

```
"usingComponents":{

    //"组件名": "路径"  列如
    "button-list": "../../../mini-program-components/button-list/button-list"

}
```
 :smirk: **1、button-list 模拟iOS列表组件** 



页面中引用

```
//wxml
    <button-list 
      bind:myevent="buttonClick"  //绑定点击事件
      wx:for="{{listArr}}"        //如果列表多的话，可以循环展示
      text="{{item.name}}"        //显示的列表名字
      img="{{item.img}}"          //名字前的图标，若不传 默认隐藏 只显示文字
      //hideRight="1"             //hideRight 是否显示右边的>号  传任意值隐藏  
    ></button-list> 

//js
//如果使用循环
    listArr:[
      {
        name:'我的会员卡',
        img:'../../utils/img/my_01@3x.png'
      },
      {
        name:'联系客服',
        img:'../../utils/img/my_02@3x.png'
      }]

buttonClick:(e) => {
    //点击事件点击之后 e.detail.type == 传入组件的text值
    console.log(e.detail.type);
    switch (e.detail.type){
      case '我的会员卡':
        //...
        break;
      //....
}
```




   :smirk: **2、city-picker 城市三级联动 选择组件**

页面中引用
```
//app.js

//判断本地是否有数据 没有 就请求
onLaunch:() => {
  if (!wx.getStorageSync('citys')) {
      wx.request({
        url: "http://restapi.amap.com/v3/config/district?&subdistrict=3&key=你的高德key",
        method: "GET",
        success: function (res) {
          console.log(res['data']['districts'][0]['districts']);
          //  请求到数据 存在本地
          wx.setStorageSync('citys', res['data']['districts'][0]['districts']);
        }
      })
    }
}

//页面中
//wxml
//绑定选中事件
<city-picker bind:selected="selectCity"></city-picker>

//js
selectCity: function(e){
  console.log(e.detail);
  //打印出来效果{province: "广西壮族自治区", city: "北海市", county: "合浦县"}
}

```